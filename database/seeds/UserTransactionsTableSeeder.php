<?php

use App\Models\User;
use App\Models\UserTransactions;
use Illuminate\Database\Seeder;

class UserTransactionsTableSeeder extends Seeder
{
    public function run()
    {
        $users = User::all();

        if ($users->isEmpty()) {
            $users = factory(User::class, 10)->create();
        }

        $users->each(function (User $user) {
            factory(UserTransactions::class, 3)->create([
                'user_id' => $user->id,
            ]);
        });
    }
}
