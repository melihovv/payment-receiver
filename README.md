# Payment Receiver

## Requirements
- git
- php>=7.1
- composer

## Installation
- `git clone https://bitbucket/melihovv/payment-receiver.git`
- `cd payment-receiver`
- `composer install`
- `cp .env.example .env`
- `php artisan key:generate`
- specify DB_DATABASE, DB_USERNAME, DB_PASSWORD variables to .env file
- `php artisan migrate`
- `php artisan db:seed --class=UsersTableSeeder` # add 10 users to database with zero balance.
- `php artisan serve`
- go to `http://localhost:8000`

## Usage
- go to `/foo?a=1&b=10&md5=1f729551f791b8808b0d0c19706cafdc` to increment balance of
user with id 1 by 10 from `foo` provider.
- go to `/bar?x=1&y=10&md5=66837a774a894a620c06c7e614e1917e` to increment balance of
user with id 1 by 10 from `bar` provider.

## Run tests
- create database `payment_receiver_testing`
- run `./vendor/bin/phpunit`

## Main classes
- [PaymentReceiver](https://bitbucket.org/melihovv/payment-receiver/src/77973c4c1ef4e9d08e01d6cc9d448a13abebbc05/app/Payment/PaymentReceiver.php?fileviewer=file-view-default)
- [FooPaymentReceiver](https://bitbucket.org/melihovv/payment-receiver/src/77973c4c1ef4e9d08e01d6cc9d448a13abebbc05/app/Payment/FooPaymentReceiver.php?fileviewer=file-view-default)
- [BarPaymentReceiver](https://bitbucket.org/melihovv/payment-receiver/src/77973c4c1ef4e9d08e01d6cc9d448a13abebbc05/app/Payment/BarPaymentReceiver.php?fileviewer=file-view-default)
- [PaymentReceiverService](https://bitbucket.org/melihovv/payment-receiver/src/77973c4c1ef4e9d08e01d6cc9d448a13abebbc05/app/Payment/PaymentReceiverService.php?fileviewer=file-view-default)
- [PaymentReceiversController](https://bitbucket.org/melihovv/payment-receiver/src/77973c4c1ef4e9d08e01d6cc9d448a13abebbc05/app/Http/Controllers/PaymentReceiversController.php?fileviewer=file-view-default)
- [AppServiceProvider](https://bitbucket.org/melihovv/payment-receiver/src/77973c4c1ef4e9d08e01d6cc9d448a13abebbc05/app/Providers/AppServiceProvider.php?fileviewer=file-view-default)
- [PaymentReceiversControllerTest](https://bitbucket.org/melihovv/payment-receiver/src/77973c4c1ef4e9d08e01d6cc9d448a13abebbc05/tests/Feature/PaymentReceiversControllerTest.php?fileviewer=file-view-default)
- [PaymentReceiverServiceTest](https://bitbucket.org/melihovv/payment-receiver/src/77973c4c1ef4e9d08e01d6cc9d448a13abebbc05/tests/Unit/Payment/PaymentReceiverServiceTest.php?fileviewer=file-view-default)
