<?php

use App\Http\Controllers\PaymentReceiversController;
use App\Http\Controllers\UserTransactionsController;

Route::get('/', UserTransactionsController::at('index'))
    ->name('user-transactions.index');

Route::get('/foo', PaymentReceiversController::at('foo'))
    ->name('payment-receiver.foo');

Route::get('/bar', PaymentReceiversController::at('bar'))
    ->name('payment-receiver.bar');
