<?php

declare(strict_types = 1);

namespace Tests\Feature;

use App\Models\User;
use App\Payment\BarPaymentReceiver;
use App\Payment\FooPaymentReceiver;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PaymentReceiversControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function it_returns_error_response_if_data_for_foo_receiver_is_not_valid()
    {
        $this
            ->get(route('payment-receiver.foo', [
                'a' => 1,
                'b' => 100,
                'md5' => 'not valid hash',
            ]))
            ->assertStatus(200)
            ->assertSee((new FooPaymentReceiver([]))->getErrorResponse());
    }

    /** @test */
    function it_increments_user_balance_if_data_for_foo_receiver_is_valid()
    {
        $user = factory(User::class)->create();

        $this
            ->get(route('payment-receiver.foo', [
                'a' => $user->id,
                'b' => 100,
                'md5' => (new FooPaymentReceiver(['a' => $user->id, 'b' => 100]))->getMd5Hash(),
            ]))
            ->assertStatus(200)
            ->assertSee((new FooPaymentReceiver([]))->getSuccessResponse());

        $this->assertEquals(100, $user->refresh()->balance);
        $this->assertCount(1, $user->transactions);
        $this->assertEquals(100, $user->transactions->first()->amount);
    }

    /** @test */
    function it_returns_error_response_if_data_for_bar_receiver_is_not_valid()
    {
        $this
            ->get(route('payment-receiver.bar', [
                'x' => 1,
                'y' => 100,
                'md5' => 'not valid hash',
            ]))
            ->assertStatus(200)
            ->assertSee((new BarPaymentReceiver([]))->getErrorResponse());
    }

    /** @test */
    function it_increments_user_balance_if_data_for_bar_receiver_is_valid()
    {
        $user = factory(User::class)->create();

        $this
            ->get(route('payment-receiver.bar', [
                'x' => $user->id,
                'y' => 100,
                'md5' => (new BarPaymentReceiver(['x' => $user->id, 'y' => 100]))->getMd5Hash(),
            ]))
            ->assertStatus(200)
            ->assertSee((new BarPaymentReceiver([]))->getSuccessResponse());

        $this->assertEquals(100, $user->refresh()->balance);
        $this->assertCount(1, $user->transactions);
        $this->assertEquals(100, $user->transactions->first()->amount);
    }
}
