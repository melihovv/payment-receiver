<?php

declare(strict_types = 1);

namespace Tests\Feature;

use App\Models\User;
use App\Models\UserTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserTransactionsControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function it_shows_user_transactions_page()
    {
        $this
            ->get(route('user-transactions.index'))
            ->assertStatus(200)
            ->assertViewIs('user-transactions')
            ->assertViewHas(['users', 'selectedUser', 'transactions']);
    }

    /** @test */
    function it_shows_user_transactions_page_for_specified_user_without_transactions()
    {
        $user = factory(User::class)->create();

        $this
            ->get(route('user-transactions.index', [
                'user_id' => $user->id,
            ]))
            ->assertStatus(200);
    }

    /** @test */
    function it_shows_user_transactions_page_for_specified_user_with_transactions()
    {
        $user = factory(User::class)->create();
        factory(UserTransactions::class, 5)->create([
            'user_id' => $user->id,
        ]);

        $this
            ->get(route('user-transactions.index', [
                'user_id' => $user->id,
            ]))
            ->assertStatus(200);
    }

    /** @test */
    function it_shows_user_transactions_page_even_if_user_id_is_not_correct()
    {
        $this
            ->get(route('user-transactions.index', [
                'user_id' => 100500,
            ]))
            ->assertStatus(200);
    }
}
