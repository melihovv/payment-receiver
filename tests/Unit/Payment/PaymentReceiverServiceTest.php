<?php

declare(strict_types = 1);

namespace Tests\Unit\Payment;

use App\Models\User;
use App\Payment\PaymentReceiver;
use App\Payment\PaymentReceiverService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Psr\Log\LoggerInterface;
use Tests\TestCase;

class PaymentReceiverServiceTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function it_returns_error_response_if_data_is_not_valid()
    {
        $receiverService = new PaymentReceiverService(new class implements PaymentReceiver {
            public function getPaymentProviderName() : string
            {
                return '';
            }

            public function getUserId()
            {
                return null;
            }

            public function getAmount() : int
            {
                return 0;
            }

            public function isValid() : bool
            {
                return false;
            }

            public function getSuccessResponse()
            {
                return '';
            }

            public function getErrorResponse()
            {
                return 'error response';
            }
        }, resolve(LoggerInterface::class));

        $this->assertEquals('error response', $receiverService->handle());
    }

    /** @test */
    function it_increments_user_balance_if_data_is_valid()
    {
        $user = factory(User::class)->create();

        $receiverService = new PaymentReceiverService(new class ($user->id) implements PaymentReceiver {
            public function __construct($userId)
            {
                $this->userId = $userId;
            }

            public function getPaymentProviderName() : string
            {
                return '';
            }

            public function getUserId()
            {
                return $this->userId;
            }

            public function getAmount() : int
            {
                return 10;
            }

            public function isValid() : bool
            {
                return true;
            }

            public function getSuccessResponse()
            {
                return 'Ok';
            }

            public function getErrorResponse()
            {
                return '';
            }
        }, resolve(LoggerInterface::class));

        $this->assertEquals('Ok', $receiverService->handle());

        $this->assertEquals(10, $user->refresh()->balance);
        $this->assertCount(1, $user->transactions);
        $this->assertEquals(10, $user->transactions->first()->amount);
    }
}
