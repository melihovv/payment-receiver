@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card-default">
                <div class="card-header">User Transactions</div>

                <div class="card-body">
                  <form action="/" method="GET">
                    <div class="form-group">
                      <label for="user_id">Select User</label>
                      <select class="form-control" id="user_id" name="user_id">
                        @foreach ($users as $user)
                          <option @if ($user->id == optional($selectedUser)->id) selected @endif
                                  value="{{ $user->id }}">
                            {{ $user->name }}
                          </option>
                        @endforeach
                      </select>
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                  </form>

                  @if ($selectedUser)
                    <div>
                      <h3>Transactions of {{ $selectedUser->name }}</h3>

                      @if ($transactions->isNotEmpty())
                        <table class="table">
                          <tr>
                            <th>Amount</th>
                            <th>Added At</th>
                          </tr>
                          @foreach ($transactions as $transaction)
                            <tr>
                              <td>{{ $transaction->amount }}</td>
                              <td>{{ $transaction->created_at }}</td>
                            </tr>
                          @endforeach
                        </table>
                      @else
                        <div>{{ $selectedUser->name }} does not have any transactions.</div>
                      @endif
                    </div>
                  @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
