<?php

namespace App\Http\Controllers;

use App\Models\User;

class UserTransactionsController extends Controller
{
    public function index()
    {
        $selectedUser = null;
        $transactions = collect();

        if (request()->user_id) {
            $selectedUser = User::find(request()->user_id);
        }

        if ($selectedUser) {
            $transactions = $selectedUser->transactions;
        }

        return view('user-transactions', [
            'users' => User::all(),
            'selectedUser' => $selectedUser,
            'transactions' => $transactions,
        ]);
    }
}
