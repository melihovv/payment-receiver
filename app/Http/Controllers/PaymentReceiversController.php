<?php

namespace App\Http\Controllers;

use App\Payment\BarPaymentReceiver;
use App\Payment\FooPaymentReceiver;
use App\Payment\PaymentReceiver;
use App\Payment\PaymentReceiverService;

class PaymentReceiversController extends Controller
{
    public function foo(FooPaymentReceiver $receiver)
    {
        return $this->handle($receiver);
    }

    public function bar(BarPaymentReceiver $receiver)
    {
        return $this->handle($receiver);
    }

    private function handle(PaymentReceiver $receiver)
    {
        return app()->makeWith(PaymentReceiverService::class, ['receiver' => $receiver])->handle();
    }
}
