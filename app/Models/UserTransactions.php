<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserTransactions extends Model
{
    use AdditionalMethods;

    protected $fillable = [
        'user_id',
        'amount',
    ];

    public function user() : BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
