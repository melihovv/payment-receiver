<?php

if (!function_exists('validate')) {
    /**
     * Validate some data.
     *
     * @param string|array $fields
     * @param string|array $rules
     * @param array $messages
     * @param array $customAttributes
     * @return bool
     */
    function validate($fields, $rules, $messages = [], $customAttributes = []) : bool
    {
        if (! is_array($fields)) {
            $fields = ['default' => $fields];
        }

        if (! is_array($rules)) {
            $rules = ['default' => $rules];
        }

        $validator = Validator::make($fields, $rules, $messages, $customAttributes);

        return $validator->passes();
    }
} else {
    throw new RuntimeException('validate function name is already taken');
}
