<?php

namespace App\Providers;

use App\Http\Controllers\PaymentReceiversController;
use App\Payment\BarPaymentReceiver;
use App\Payment\FooPaymentReceiver;
use App\Payment\PaymentReceiverService;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(FooPaymentReceiver::class, function (Application $app) {
            return new FooPaymentReceiver($app['request']->all());
        });

        $this->app->bind(BarPaymentReceiver::class, function (Application $app) {
            return new BarPaymentReceiver($app['request']->all());
        });
    }
}
