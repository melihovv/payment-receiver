<?php

declare(strict_types = 1);

namespace App\Payment;

class FooPaymentReceiver implements PaymentReceiver
{
    private const SALT = 'foo';

    /**
     * @var array
     */
    private $params;

    /**
     * @param array $params
     */
    public function __construct(array $params)
    {
        $this->params = $params;
    }

    /**
     * @inheritdoc
     */
    public function getPaymentProviderName() : string
    {
        return 'foo';
    }

    /**
     * @inheritdoc
     */
    public function getUserId()
    {
        return $this->params['a'];
    }

    /**
     * @inheritdoc
     */
    public function getAmount() : int
    {
        return (int)$this->params['b'];
    }

    /**
     * @inheritdoc
     */
    public function isValid() : bool
    {
        return validate($this->params, [
            'a' => 'required|exists:users,id',
            'b' => 'required|integer',
            'md5' => [
                'required',
                function ($attribute, $value) {
                    return $value === $this->getMd5Hash();
                },
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getSuccessResponse()
    {
        return '<?xml version="1.0" encoding="UTF-8" standalone="no"?><answer>1</answer>';
    }

    /**
     * @inheritdoc
     */
    public function getErrorResponse()
    {
        return '<?xml version="1.0" encoding="UTF-8" standalone="no"?><answer>0</answer>';
    }

    /**
     * @return string
     */
    public function getMd5Hash() : string
    {
        return md5(($this->params['a'] ?? '') . ($this->params['b'] ?? '') . self::SALT);
    }
}
