<?php

declare(strict_types = 1);

namespace App\Payment;

class BarPaymentReceiver implements PaymentReceiver
{
    private const SALT = 'bar';

    /**
     * @var array
     */
    private $params;

    /**
     * @param array $params
     */
    public function __construct(array $params)
    {
        $this->params = $params;
    }

    /**
     * @inheritdoc
     */
    public function getPaymentProviderName() : string
    {
        return 'bar';
    }

    /**
     * @inheritdoc
     */
    public function getUserId()
    {
        return $this->params['x'];
    }

    /**
     * @inheritdoc
     */
    public function getAmount() : int
    {
        return (int)$this->params['y'];
    }

    /**
     * @inheritdoc
     */
    public function isValid() : bool
    {
        return validate($this->params, [
            'x' => 'required|exists:users,id',
            'y' => 'required|integer',
            'md5' => [
                'required',
                function ($value) {
                    return $value === $this->getMd5Hash();
                },
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getSuccessResponse()
    {
        return 'OK';
    }

    /**
     * @inheritdoc
     */
    public function getErrorResponse()
    {
        return 'ERROR';
    }

    /**
     * @return string
     */
    public function getMd5Hash() : string
    {
        return md5(($this->params['x'] ?? '') . ($this->params['y'] ?? '') . self::SALT);
    }
}
