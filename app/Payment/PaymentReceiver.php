<?php

declare(strict_types = 1);

namespace App\Payment;

interface PaymentReceiver
{
    /**
     * @return string
     */
    public function getPaymentProviderName() : string;

    /**
     * @return mixed
     */
    public function getUserId();

    /**
     * Get payment amount.
     *
     * @return integer
     */
    public function getAmount() : int;

    /**
     * Whether is data provided to this payment receiver valid?
     *
     * @return bool
     */
    public function isValid() : bool;

    /**
     * Get success response.
     *
     * @return mixed
     */
    public function getSuccessResponse();

    /**
     * Get error response.
     *
     * @return mixed
     */
    public function getErrorResponse();
}
