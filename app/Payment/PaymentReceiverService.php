<?php

declare(strict_types = 1);

namespace App\Payment;

use App\Models\User;
use Illuminate\Support\Facades\DB;
use Psr\Log\LoggerInterface;
use Throwable;

class PaymentReceiverService
{
    /** @var PaymentReceiver */
    private $receiver;

    /** @var LoggerInterface */
    private $logger;

    public function __construct(PaymentReceiver $receiver, LoggerInterface $logger)
    {
        $this->receiver = $receiver;
        $this->logger = $logger;
    }

    /**
     * @return mixed
     */
    public function handle()
    {
        if (!$this->receiver->isValid()) {
            $this->logger->info("Got not valid data from {$this->receiver->getPaymentProviderName()} provider.");

            return $this->receiver->getErrorResponse();
        }

        $this->makeTransaction();

        return $this->receiver->getSuccessResponse();
    }

    private function makeTransaction() : void
    {
        DB::beginTransaction();

        try {
            $userId = $this->receiver->getUserId();
            $user = User::lockForUpdate()->find($userId);

            $amount = $this->receiver->getAmount();
            $this->logger->info("Increment balance of user with id {$userId} by {$amount}.");
            $user->increment('balance', $amount);

            $this->logger->info("Add transaction with amount {$amount} for user with id {$userId}.");
            $user->transactions()->create([
                'amount' => $amount,
            ]);

            DB::commit();

            $this->logger->info("Data from {$this->receiver->getPaymentProviderName()} provider was successfully handled.");
        } catch (Throwable $e) {
            DB::rollBack();

            $this->logger->error("There was an error during data handling from {$this->receiver->getPaymentProviderName()} provider.");

            report($e);

            throw $e;
        }
    }
}
